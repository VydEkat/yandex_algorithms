# В данном списке из n ≤ 10^5 целых чисел найдите три числа, произведение
# которых максимально.

# Решение должно иметь сложность О(п), где п - размер списка.

# Выведите тои искомых числа в любом порядке.

def sort(seq, left, right, k):
    left = 0
    right = len(seq) - 1
    while left < right:
        x = seq[(right + left) // 2]
        eqxfirst = left
        bigxfirst = left
        for i in range(left, right + 1):
            now = seq[i]
            if now == x:
                seq[i] = seq[bigxfirst]
                seq[bigxfirst] = now
                bigxfirst += 1
            elif now < x:
                seq[i] = seq[bigxfirst]
                seq[bigxfirst] = seq[eqxfirst]
                seq[eqxfirst] = now
                eqxfirst += 1
                bigxfirst += 1
        if k < eqxfirst:
            right = eqxfirst - 1
        elif k >= bigxfirst:
            left = bigxfirst
        else:
            return

seq = list(map(int, input().split()))
sort(seq, 0, len(seq), len(seq) - 1)
sort(seq, 0, len(seq) - 1, len(seq) - 2)
sort(seq, 0, len(seq) - 3, 2)

if seq[-1] * seq[-2] * seq[-3] >= seq[-1] * seq[0] * seq[1]:
    print(seq[-1], seq[-2], seq[-3])
else:
    print(seq[-1], seq[0], seq[2])