# За многие годы заточения узник замка Иф проделал в стене
# прямоугольное отверстие размером D × Е. Замок Иф сложен из кирпичей, размером А × В × С. Определите, сможет ли узник выбрасывать кирпичи в море через это отверстие, если стороны кирпича должны быть параллельны сторонам отверстия.

a = int(input())
b = int(input())
c = int(input())
d = int(input())
e = int(input())

def sort2(first, second):
    if first < second:
        return (first, second)
    return (second, first)

a, b = sort2(a, b)
b, c = sort2(b, c)
a, b = sort2(a, b)
d, e = sort2(d, e)

if a <= d and e <= b:
    print('YES')
else:
    print('NO') 